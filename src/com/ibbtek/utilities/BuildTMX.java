/*
* The MIT License
*
* XLS(X)toTMX
*
* Copyright 2015 Ibbtek <http://ibbtek.altervista.org/>.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

package com.ibbtek.utilities;

import static com.ibbtek.xls.x.totmx.MainGui.xls;
import static com.ibbtek.xls.x.totmx.MainGui.tmx;
import static com.ibbtek.utilities.LogToFile.log;
import static com.ibbtek.xls.x.totmx.MainGui.jProgressBar1;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.w3c.dom.Attr;

/**
 * BuildTMX class
 * @author Ibbtek <http://ibbtek.altervista.org/>
 */
public class BuildTMX implements Runnable{
    List<String> columns = new ArrayList<>();
    Iterator<Row> rowIterator;
    Attr attr;
    Document doc;
    Element header;
    Element body;
    
    @Override
    public void run(){
        FileInputStream fis = null;
        boolean res=true;
        try {
            //Get XLX(X) file
            fis = new FileInputStream(xls);
            String ext = xls.getPath().substring(xls.getPath().lastIndexOf(".")+1);
            
            //Clear TMX file if exist
            res=clearFile(tmx);
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
 
            // root element
            doc = docBuilder.newDocument();
            Element root = doc.createElement("tmx");
            doc.appendChild(root);
            
            // set attribute to root element
            attr = doc.createAttribute("version");
            attr.setValue("1.1");
            root.setAttributeNode(attr);
            
            // header element
            header = doc.createElement("header");
            root.appendChild(header);
            
            // set attributes to header element
            attr = doc.createAttribute("creationtool");
            attr.setValue("XLS(X)toTMX");
            header.setAttributeNode(attr);
            attr = doc.createAttribute("o-tmf");
            attr.setValue("XLS(X)toTMX");
            header.setAttributeNode(attr);
            attr = doc.createAttribute("adminlang");
            attr.setValue("EN-US");
            header.setAttributeNode(attr);
            attr = doc.createAttribute("datatype");
            attr.setValue("plaintext");
            header.setAttributeNode(attr);
            attr = doc.createAttribute("creationtoolversion");
            attr.setValue("1.0");
            header.setAttributeNode(attr);
            attr = doc.createAttribute("segtype");
            attr.setValue("sentence");
            header.setAttributeNode(attr);
            
            // body element
            body = doc.createElement("body");
            root.appendChild(body);
            
            //Check if the file is XLS or XLSX extension
            if(ext.equalsIgnoreCase("xls")){
                
                // Finds the workbook instance for XLS file
                HSSFWorkbook myWorkBook = new HSSFWorkbook (fis);
                
                // Return first sheet from the XLS workbook
                HSSFSheet mySheet = myWorkBook.getSheetAt(0);
                
                // Get iterator to all the rows in current sheet
                rowIterator = mySheet.iterator();
                
                res=fillFile();
                
            }else if(ext.equalsIgnoreCase("xlsx")){
                // Finds the workbook instance for XLSX file
                XSSFWorkbook myWorkBook = new XSSFWorkbook (fis);
                
                // Return first sheet from the XLSX workbook
                XSSFSheet mySheet = myWorkBook.getSheetAt(0);
                
                // Get iterator to all the rows in current sheet
                rowIterator = mySheet.iterator();
                
                res=fillFile();
                
            }
            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(tmx);

            // Output to console for testing
            //StreamResult result = new StreamResult(System.out);

            transformer.transform(source, result);
        
        } catch (FileNotFoundException ex) {
            log(ex,"config","Can't open the XLS(X) file.");
            Thread.currentThread().interrupt();
            res=false;
        } catch (IOException ex) {
            log(ex,"config","Can't read the XLS(X) file.");
            Thread.currentThread().interrupt();
            res=false;
        } catch (TransformerException | ParserConfigurationException ex) {
            log(ex,"config","Can't convert the XLS(X) file to TMX file.");
            Thread.currentThread().interrupt();
            res=false;
        }finally{
            try {

                fis.close();
            } catch (IOException ex) {
                log(ex,"config","Can't close XLS(X) file.");
            }
        }
        
        if(res){
            JOptionPane.showMessageDialog(null,"Convertion successful",
                    "Info", JOptionPane.INFORMATION_MESSAGE);
        }else{
            JOptionPane.showMessageDialog(null,"Convertion aborted, check the log file",
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    /**
     * clearFile Method
     * That clear completly the file
     * @param file
     * @return boolean to say if something wrong happend
     */
    private boolean clearFile(File file){
        boolean result=true;
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(file);
            writer.print("");
        } catch (FileNotFoundException ex) {
            log(ex,"config","Can't clear TMX file.");
            result=false;
        }finally{
            writer.close();
        }
        return result;
    }
    
    private boolean fillFile(){
        
        // Traversing over each row of XLSX file
        while (rowIterator.hasNext()) {
                    
            Row row = rowIterator.next();
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
                return false;
            }
            /*
            Increment the progress bar and get the first line of the CSV
            file
            */
            jProgressBar1.setValue(row.getRowNum());

            if(row.getRowNum()==0){
                // For each row, iterate through each columns
                Iterator<Cell> cellIterator = row.cellIterator();
                while (cellIterator.hasNext()) {

                    Cell cell = cellIterator.next();

                    switch (cell.getCellType()) {
                        case Cell.CELL_TYPE_STRING:
                            columns.add(cell.getStringCellValue());
                            break;
                        case Cell.CELL_TYPE_NUMERIC:
                            columns.add(String.valueOf(cell.getNumericCellValue()));
                            break;
                        case Cell.CELL_TYPE_BOOLEAN:
                            columns.add(String.valueOf(cell.getBooleanCellValue()));
                            break;
                        default :
                    }
                }
                attr = doc.createAttribute("srclang");
                attr.setValue(columns.get(0));
                header.setAttributeNode(attr);
            }else{
                // Add <tu> element
                Element tu = doc.createElement("tu");
                body.appendChild(tu);

                // For each row, iterate through each columns
                Iterator<Cell> cellIterator = row.cellIterator();
                while (cellIterator.hasNext()) {

                    Cell cell = cellIterator.next();

                    switch (cell.getCellType()) {
                        case Cell.CELL_TYPE_STRING:
                            if(cell.getColumnIndex()==0){
                                // Add <tuv> element
                                Element tuv = doc.createElement("tuv");
                                tu.appendChild(tuv);

                                // set attributes to <tuv> element
                                attr = doc.createAttribute("lang");
                                attr.setValue(columns.get(0));
                                tuv.setAttributeNode(attr);

                                // Add <seg> element
                                Element seg = doc.createElement("seg");
                                seg.appendChild(doc.createTextNode(cell.getStringCellValue()));
                                tuv.appendChild(seg);
                            }else if(cell.getColumnIndex()==1){
                                // Add <tuv> element
                                Element tuv = doc.createElement("tuv");
                                tu.appendChild(tuv);

                                // set attributes to <tuv> element
                                attr = doc.createAttribute("lang");
                                attr.setValue(columns.get(1));
                                tuv.setAttributeNode(attr);

                                // Add <seg> element
                                Element seg = doc.createElement("seg");
                                seg.appendChild(doc.createTextNode(cell.getStringCellValue()));
                                tuv.appendChild(seg);
                            }else{
                                // set attributes to <tu> element
                                attr = doc.createAttribute(columns.get(cell.getColumnIndex()));
                                attr.setValue(cell.getStringCellValue());
                                tu.setAttributeNode(attr);
                            }
                            break;
                        case Cell.CELL_TYPE_NUMERIC:
                            if(cell.getColumnIndex()==0){
                                // Add <tuv> element
                                Element tuv = doc.createElement("tuv");
                                tu.appendChild(tuv);

                                // set attributes to <tuv> element
                                attr = doc.createAttribute("lang");
                                attr.setValue(columns.get(0));
                                tuv.setAttributeNode(attr);

                                // Add <seg> element
                                Element seg = doc.createElement("seg");
                                seg.appendChild(doc.createTextNode(String.valueOf(cell.getNumericCellValue())));
                                tuv.appendChild(tuv);
                            }else if(cell.getColumnIndex()==1){
                                // Add <tuv> element
                                Element tuv = doc.createElement("tuv");
                                tu.appendChild(tuv);

                                // set attributes to <tuv> element
                                attr = doc.createAttribute("lang");
                                attr.setValue(columns.get(1));
                                tuv.setAttributeNode(attr);

                                // Add <seg> element
                                Element seg = doc.createElement("seg");
                                seg.appendChild(doc.createTextNode(String.valueOf(cell.getNumericCellValue())));
                                tuv.appendChild(tuv);
                            }else{
                                // set attributes to <tu> element
                                attr = doc.createAttribute(columns.get(cell.getColumnIndex()));
                                attr.setValue(String.valueOf(cell.getNumericCellValue()));
                                tu.setAttributeNode(attr);
                            }
                            break;
                        case Cell.CELL_TYPE_BOOLEAN:
                            if(cell.getColumnIndex()==0){
                                // Add <tuv> element
                                Element tuv = doc.createElement("tuv");
                                tu.appendChild(tuv);

                                // set attributes to <tuv> element
                                attr = doc.createAttribute("lang");
                                attr.setValue(columns.get(0));
                                tuv.setAttributeNode(attr);

                                // Add <seg> element
                                Element seg = doc.createElement("seg");
                                seg.appendChild(doc.createTextNode(String.valueOf(cell.getNumericCellValue())));
                                tuv.appendChild(tuv);
                            }else if(cell.getColumnIndex()==1){
                                // Add <tuv> element
                                Element tuv = doc.createElement("tuv");
                                tu.appendChild(tuv);

                                // set attributes to <tuv> element
                                attr = doc.createAttribute("lang");
                                attr.setValue(columns.get(1));
                                tuv.setAttributeNode(attr);

                                // Add <seg> element
                                Element seg = doc.createElement("seg");
                                seg.appendChild(doc.createTextNode(String.valueOf(cell.getNumericCellValue())));
                                tuv.appendChild(tuv);
                            }else{
                                // set attributes to <tu> element
                                attr = doc.createAttribute(columns.get(cell.getColumnIndex()));
                                attr.setValue(String.valueOf(cell.getNumericCellValue()));
                                tu.setAttributeNode(attr);
                            }
                            break;
                        default :
                    }
                }
            }
        }
        return true;
    }
    
}
