# XLSXTOTMX

XLSX2TMX allows you to convert a XLS(X) [Office 2003/2007 or Office 2010/2013] Excel file into a TMX file format in seconds. The TMX file format is a Translation Memory Exchange file widely use in some famous CAT tools like OmegaT.

Most of those tools can help you to create those TMX files through a creating interface, but that process can be very long and painful if you have many entries to insert.

This software give you a way to avoid that process by creating those TMX files from a simple Excel file that you can fill in really quickly.

This is an old project that is not maintained anymore.

Feel free to fork it and do whatever you want with it.

It was developped with the NetBeans IDE

Under [MIT License](LICENSE.md)
